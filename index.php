<?php
ini_set('max_execution_time', '72000');
ignore_user_abort(true);
set_time_limit(0);
include __DIR__ . '/App/Bootstrap.php';

include __DIR__ . '/vendor/autoload.php';

if(session_status() !== PHP_SESSION_ACTIVE) {
	session_start();
}


$core = new Core();