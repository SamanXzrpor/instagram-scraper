<?php
namespace App\Services;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \App\Helpers\HelpUs;
class ExcelHandler extends Spreadsheet
{

    public static function getDataOfExcel($type , $file)
    {
        $accountData = [];

        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::CreateReader($type);
        $spreadsheet = $reader->load($file);
        $excelData = $spreadsheet->getActiveSheet()->toArray();

        foreach ($excelData as $data){
            
            $accountData[] =  array_slice($data, 0,2);
            
        }
        
        return $accountData;
    }

    public static function createExcel($data)
    {
        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1' , 'تگ کننده');
        $sheet->setCellValue('B1' , 'تگ شده' );
        $sheet->setCellValue('C1' , 'نوع تبلیغات');
        $sheet->setCellValue('D1' , 'تعداد لایک');
        $sheet->setCellValue('E1' , 'تعداد کامنت');
        $sheet->setCellValue('F1' , 'تعداد ویو');

        foreach ($data as $key => $value) {
            $sheet->setCellValue('A'.$key+2 , $value->owner);
            $sheet->setCellValue('B'.$key+2 , $value->propagend_tag);
            $sheet->setCellValue('C'.$key+2 , $value->type);
            $sheet->setCellValue('D'.$key+2 , $value->like_count);
            $sheet->setCellValue('E'.$key+2 , $value->comment_count);
            $sheet->setCellValue('F'.$key+2 , $value->video_view);
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save(HelpUs::getConfigs('Url.basePath') . 'Download/excel.xlsx');
    }

}
