<?php
namespace App\Services;

use \App\Services\Tools\ValidatePropagendStories;
use \App\Utilities\ImageColorize;
use \App\Utilities\Downloader;
use \Phpfastcache\Helper\Psr16Adapter;
use \GuzzleHttp\Client;
use \App\Services\Tools\GetUserIDs;
use \App\Models\Propagend;
use \App\Helpers\HelpUs;
use Exception;

class GetPropagendTag 
{

    public const TPMD_VIDEO   = 'video';
    public const TPMD_IMG     = 'image';
    public const TPMD_SIDECAR = 'sidecar';
    public const TP_POST      = 'post';
    public const TP_STORY     = 'story';

    protected $userName ;
    protected $password ;
    protected $instagram ;
    private   $client;
    private   $accountsData = [];

    public function __construct(string $proxy = null, array $myAcc = [], array $accountsData = [])
    {
        
        $this->client = new Client([
            'base_uri' => 'https://www.instagram.com/',
            'connect_timeout' => 20,
            'read_timeout' => 25,
            'timeout' => 22.14,
            'request.options' => [
                'proxy' => 'tcp://'. $proxy ,
            ],
        ]);
    
        
        $this->instagram = \InstagramScraper\Instagram::withCredentials($this->client,$myAcc[0] ,$myAcc[1], new Psr16Adapter('Files'));
        $this->instagram->login(); // will use cached session if you want to force login $instagram->login(true)
        $this->instagram->saveSession();  //DO NOT forget this in order to save the session, otherwise have no sense
         
        $this->accountsData = $accountsData;

     
    }


    public function posts() 
    {
        $saveDatabase = new Propagend;

        foreach ($this->accountsData as $account) {
            
            if(is_null($account[0]))
                continue; 
            
            if(is_null($this->instagram->getAccount($account[0])))
                continue;

            if($this->instagram->getAccount($account[0])->isPrivate())
                $this->instagram->follow($this->instagram->getAccount($account[0])->getId());


            try {
                $medias = $this->instagram->getMedias($account[0] , 12);
            } catch (Exception $e) {
                die($e->getMessage());
            }
            
            foreach ($medias as $media) {
                
                # Check Media Exist in Database Or notExist
                $ID     = $media->getId();
                $existID = $saveDatabase->getOneMeidaByID($ID);

                if(empty($existID)){

                    $captionOfPost = $media->getCaption();

                    # Validate that this post is a prppagend Post or Not
                    $propagendaPatternTag = '/@[a-zA-Z0-9.\-_]+/';
                    preg_match($propagendaPatternTag , $captionOfPost , $match);

                    if(!empty($match[0]) AND strtolower($match[0]) !== '@'.$account[0]){
                   
                        # Listed Post by Type of theim {One Video , One Image or SideCar Post}
                        switch ($media->getType()) {
                            case 'video':
                                $saveDatabase->addMedia($ID ,self::TP_POST , self::TPMD_VIDEO , $match[0]
                                ,$account[0] , $media->getVideoViews() ,$media->getLikesCount() ,$media->getCommentsCount() , $media->getCreatedTime(),);
                                break;
                            case 'image':
                                $saveDatabase->addMedia($ID ,self::TP_POST , self::TPMD_IMG , $match[0] ,$account[0] 
                                , '' ,$media->getLikesCount()  ,$media->getCommentsCount() , $media->getCreatedTime());
                                break;
                            case 'sidecar':

                                $saveDatabase->addMedia($ID ,self::TP_POST , self::TPMD_SIDECAR , $match[0] ,$account[0]
                                , $media->getVideoViews(),$media->getLikesCount() , $media->getCommentsCount() , $media->getCreatedTime());
                                break;
                        }
                    }
                }
            }      
        }
    }


    public function stories() 
    {
        $saveDatabase = new Propagend();
        $getDataId    = new GetUserIDs();
        $IDs = $getDataId->getData($this->instagram , $this->accountsData);

        try {
            $storiesAcc = $this->instagram->getStories($IDs);
        } catch (Exception $e) {
            die($e->getMessage());
        }
         
        foreach($storiesAcc as $acc)
        {     

            foreach ($acc->getStories() as $story)
            {
                # Check Media Exist in Database Or notExist
                $ID          = $story->getId();
                $existID = $saveDatabase->getOneMeidaByID($ID);

                if(empty($existID)){

                    # Set Path for fetch and give images 
                    $path  = HelpUs::getConfigs('Url.basePath') . 'Download/Validation/download.png'; 
                    $path2 = HelpUs::getConfigs('Url.basePath') . 'Download/Validation/pattern.png'; 

                    # Get story url 
                    $storyThumnail = $story->imageStandardResolutionUrl;
                    $storyThumnail = $story->getImageStandardResolutionUrl();

                    sleep(1);
                    if(filter_var($storyThumnail ,FILTER_VALIDATE_URL)){
                        Downloader::downloadURL($storyThumnail);
                    }
                    # Change Color Image to gray Scale 
                    ImageColorize::colorize($path);

                    # Verification Stories and get match
                    $match = ValidatePropagendStories::validate( $path2 );
                    
                    if(!empty($match)){
                                    
                        if($story->getType() == 'video'){
                            $path = Downloader::downloadFile($story->videoStandardResolutionUrl , self::TP_STORY , self::TPMD_VIDEO);
                            $saveDatabase->addMedia($ID ,self::TP_STORY , self::TPMD_VIDEO , $match[0] ,$acc->ownegetOwner()->username ,'','','',$story->getCreatedTime());
                        }else{
                            $path = Downloader::downloadFile($story->imageStandardResolutionUrl , self::TP_STORY , self::TPMD_IMG);
                            $saveDatabase->addMedia($ID ,self::TP_STORY , self::TPMD_IMG , $match[0] ,$acc->getOwner()->username ,'','','',$story->getCreatedTime());
                        }
                    }
                }
            }
        }
    }


}