<?php
namespace App\Services\Tools;

use \thiagoalessio\TesseractOCR\TesseractOCR;
use \App\Helpers\HelpUs;
class ValidatePropagendStories
{

    public static function validate( $path )
    {
        
        $ocr = new TesseractOCR($path);
        //$ocr->executable(HelpUs::getConfigs('Url.basePath') .'Tesseract-OCR/tesseract.exe');
       
        $ocr->lang('eng');
        $ocr->psm(6);
        $ocr->dpi(300);
        $text = $ocr->run();
        $propagendaTag = '/@[A-z][a-zA-Z0-9.\-_]+/';
        preg_match($propagendaTag , $text , $match);
     
        if(!empty($match) && strlen($match[0]) >= 4){
            
            return $match;
        }   
    }
}

