<?php
namespace App\Services\Tools;

use \InstagramScraper\Instagram;
use Exception;

class GetUserIDs 
{
    public function getData(Instagram $instagram ,array $accountsData)
    {
        foreach ($accountsData as $account)
        {
            if(is_null($account[0]))
                continue;
            if(is_null($instagram->getAccount($account[0])))
                continue;
    
            try {
                $this->IDs[] = $instagram->getAccount($account[0])->getId();
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }
    }
}