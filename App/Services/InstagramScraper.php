<?php
namespace App\Services;

use \App\Helpers\HelpUs;
use \Phpfastcache\Helper\Psr16Adapter;
use \GuzzleHttp\Client;
use \App\Models\Media;
use \App\Services\Tools\GetUserIDs;
use \App\Services\Tools\ValidatePropagendStories;
use \App\Utilities\ImageColorize;
use \App\Utilities\Downloader;
use Exception;

class InstagramScraper
{

    public int $proxyCounter  = 0;
    public const TPMD_VIDEO   = 'video';
    public const TPMD_IMG     = 'image';
    public const TPMD_SIDECAR = 'sidecar';
    public const TP_POST      = 'post';
    public const TP_STORY     = 'story';

    private static $proxies = [
        '195.137.203.242:80',
        '77.89.233.54:8080',
        '187.92.84.146:80',
        '193.239.38.117:8080',
        '210.14.70.21:81',
        '201.73.178.101:80',
        '95.104.116.227:80',
        '213.192.55.162:8080',
        '204.15.150.126:8080',
        '81.0.235.165:80',
        '218.29.54.105:80',
        '171.66.247.193:80',
        '173.254.238.26:80',
        '180.211.162.162:8080',
        '101.230.8.69:8000',
        '202.152.49.236:8080',
        '202.159.6.146:80',
        '203.86.16.236:8080',
        '167.114.67.197:80',
        '190.210.186.241:80',
        '202.4.186.102:80',
        '94.183.244.163:8080',
        '199.168.148.90:443',
        '217.195.215.217:8080',
        '45.64.99.229:8080',
        '54.84.159.217:80',
        '117.59.224.106:80',
    ];

    protected $instagram ;
    protected $mediaLinks = [];
    private   $client;
    private   $IDs = [];
    private   $accountsData = [];

    public function __construct(string $proxy = null, array $myAcc = [], array $accountsData = [])
    {

        $this->client = new Client([
            'base_uri' => 'https://www.instagram.com/',
            'connect_timeout' => 25,
            'read_timeout' => 25,
            'timeout' => 25.14,
            'request.options' => [
                'proxy' => 'tcp://'. $proxy ,
            ],
        ]);

        $this->instagram = \InstagramScraper\Instagram::withCredentials($this->client,$myAcc[0] ,$myAcc[1],new Psr16Adapter('Files'));
      
        $this->instagram->login(); // will use cached session if you want to force login $this->instagram->login(true)
        $this->instagram->saveSession();  //DO NOT forget this in order to save the session, otherwise have no sense
         
        $this->accountsData = $accountsData;
        
    }


    public function posts() 
    {
        $saveDatabase = new Media;

        foreach ($this->accountsData as $account) {
            
            if(is_null($account[0]))
                continue; 
            
            if(is_null($this->instagram->getAccount($account[0])))
                continue;

            if($this->instagram->getAccount($account[0])->isPrivate())
                $this->instagram->follow($this->instagram->getAccount($account[0])->getId());

            sleep(1);

            try {
                $medias = $this->instagram->getMedias($account[0] , 12);
            } catch (Exception $e) {
                die($e->getMessage());
            }
            
            foreach ($medias as $media) {

                $like = $media->getLikesCount();
                $comm = $media->getCommentsCount();
                $view = $media->getVideoViews();
                $time = $media->getCreatedTime();
                
                # Check Media Exist in Database Or notExist
                $ID      = $media->getID();
                $existID = $saveDatabase->getOneMeidaByID($ID);

                if(empty($existID)){

                    $captionOfPost = $media->getCaption();

                    # Validate that this post is a prppagend Post or Not
                    $propagendaPatternTag = '/@[a-zA-Z0-9.\-_]+/';
                    preg_match($propagendaPatternTag , $captionOfPost , $match);

                    if(!empty($match[0]) AND strtolower($match[0]) !== '@'.$account[0]){
                        
                        # Ditermine Limit of like Posts
                        $limitLike = !empty($account[1]) ?  $account[1] : 20000;
                        if($media->getLikesCount() > intval($limitLike) / 2.5){
                                            
                            # Listed Post by Type of theim {One Video , One Image or SideCar Post}
                            switch ($media->getType()) {
                                case 'video':
                                    $coverPath = 'Posts/' .Downloader::downloadFile($media->getImageStandardResolutionUrl() , self::TP_POST , self::TPMD_IMG);
                                    $path      = 'Posts/' . Downloader::downloadFile($media->getVideoStandardResolutionUrl() , self::TP_POST , self::TPMD_VIDEO);
                                    $saveDatabase->addMedia($ID ,$path , $coverPath,self::TP_POST , self::TPMD_VIDEO , $match[0]
                                    ,$account[0] , $view ,$like ,$comm , (time() - $time));
                                    break;
                                case 'image':
                                    $path = 'Posts/' .Downloader::downloadFile($media->getImageStandardResolutionUrl() , self::TP_POST , self::TPMD_IMG);
                                    $saveDatabase->addMedia($ID ,$path , $path,self::TP_POST , self::TPMD_IMG , $match[0] ,$account[0] 
                                    , '' ,$like ,$comm , (time() - $time));
                                    break;
                                case 'sidecar':

                                    $sidecarMedia = [];

                                    $coverPath = 'Posts/' .Downloader::downloadFile($media->getImageStandardResolutionUrl() , self::TP_POST , self::TPMD_IMG);
                                    
                                    foreach ($media->getSidecarMedias() as $media){
                                        
                                        if($media->getType() === 'video'){
                                            $path = 'Posts/' .Downloader::downloadFile($media->getVideoStandardResolutionUrl() , self::TP_POST , self::TPMD_VIDEO);
                                            $sidecarMedia[] = $path;
                                        }else{
                                            $path = 'Posts/' .Downloader::downloadFile($media->getImageStandardResolutionUrl() , self::TP_POST , self::TPMD_IMG);
                                            $sidecarMedia[] = $path;
                                        }
                                    }
                                    $saveDatabase->addMedia($ID ,serialize($sidecarMedia) , $coverPath ,self::TP_POST , self::TPMD_SIDECAR , $match[0],$account[0]
                                    , $view ,$like ,$comm , (time() - $time));
                                    break;
                            }
                        }
                    }
                }      
            }
        }
    }

    public function stories() 
    {
       
        $saveDatabase = new Media();
        $getDataId    = new GetUserIDs();
        $IDs = $getDataId->getData($this->instagram , $this->accountsData);
      
        try {
            $storiesAcc = $this->instagram->getStories($IDs);
        } catch (Exception $e) {
            die($e->getMessage());
        }
         
        foreach($storiesAcc as $acc)
        {     

            foreach ($acc->getStories() as $story)
            {
                $time = $story->getCreatedTime();
                # Check Media Exist in Database Or notExist
                $ID          = $story->getID();
                $existID = $saveDatabase->getOneMeidaByID($ID);

                if(empty($existID)){

                    # Set Path for fetch and give images 
                    $path  = HelpUs::getConfigs('Url.basePath') . 'Download/Validation/download.png'; 
                    $path2 = HelpUs::getConfigs('Url.basePath') . 'Download/Validation/pattern.png'; 

                    # Get story url 
                    $storyThumnail = $story->getImageStandardResolutionUrl();

                    sleep(1);
                    if(filter_var($storyThumnail ,FILTER_VALIDATE_URL)){
                        Downloader::downloadURL($storyThumnail);
                    }
                    # Change Color Image to gray Scale 
                    ImageColorize::colorize($path);

                    # Verification Stories and get match
                    $match = ValidatePropagendStories::validate( $path2 );
                    
                    if(!empty($match[0])){
     
                        if($story->getType() == 'video'){
                            $coverPath = 'Stories/' .Downloader::downloadFile($story->getImageStandardResolutionUrl() , self::TP_POST , self::TPMD_IMG);
                            $path = 'Stories/' .Downloader::downloadFile($story->getVideoStandardResolutionUrl() , self::TP_STORY , self::TPMD_VIDEO);
                            $saveDatabase->addMedia($ID ,$path ,$coverPath ,self::TP_STORY , self::TPMD_VIDEO , $match[0] ,$acc->getOwner()->username ,'0','0','0',(time() - $time));
                        }else{
                            $path = 'Stories/' .Downloader::downloadFile($story->getImageStandardResolutionUrl() , self::TP_STORY , self::TPMD_IMG);
                            $saveDatabase->addMedia($ID ,$path , $path, self::TP_STORY , self::TPMD_IMG , $match[0] ,$acc->getOwner()->username ,'0','0','0',(time() - $time));
                        }
                    }
                }
            }
        }
    }



    public function changeProxy()
    {
        $proxy = HelpUs::Changer(self::$proxies, $this->proxyCounter);

        $this->client = new Client([
            'base_uri' => 'https://www.instagram.com/',
            'timeout' => 30.0,
            'cookie' => true,
            'verify' => false,
            'read_timeout' => 30,
            'request.options' => [
                'proxy' => 'tcp://' . $proxy,
            ]
        ]);
    }
}