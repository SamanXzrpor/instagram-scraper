<?php
namespace App\Utilities;

use App\Helpers\HelpUs;
use \Colorizzar\ChangeColor;


class ImageColorize 
{

    public static function colorize( $imagePath )
    {
        $imagePath2 =  HelpUs::getConfigs('Url.basePath') . 'Download/Validation/pattern.png';
      
        if(empty(file_get_contents($imagePath))){
            $imagePath = HelpUs::getConfigs('Url.basePath') . 'Download/Validation/notExist.png';
        }


        // $changeColor = new ChangeColor('red_car.png');

        // $changeColor->setFromHex('#FF1F28');

        // $changeColor->colorizeByNameColor('Blue', 'new_cars/');

        $im = new \Imagick(realpath($imagePath));
        $im->blackthresholdimage('dimgray');
        $im->embossImage(5 , 0.5);
        $im->setFormat('png');

        if(!file_exists($imagePath2)){
            file_put_contents($imagePath2 , '');
        }

        $im->writeImage($imagePath2);
        // header('Content-Type: image/png');
        // echo $im->getImageBlob();


        
    }
}