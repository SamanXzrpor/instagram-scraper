<?php
namespace App\Utilities;

class Pagination 
{

    public static function paginate ($type)
    {
        $page = 1 ;
        
        if ($type == 'previous') {
            $page = $page - 1 ;
        }

        if ($type == 'next') {
            $page = $page + 1 ;
        }

        return $page ;
    }

}