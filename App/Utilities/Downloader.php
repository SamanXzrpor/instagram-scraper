<?php
namespace App\Utilities;

use \App\Helpers\HelpUs;
use \greeflas\tools\ImageDownloader;
use \greeflas\tools\validators\ImageValidator;

class Downloader
{

    public static function downloadURL( $url )
    {
        $imagesRoot =  HelpUs::getConfigs('Url.basePath') . 'Download/Validation/';

        $downloader = new ImageDownloader([
            'class' => ImageValidator::class
        ]);
        
        if(!file_exists($imagesRoot . 'download.png'))
            file_put_contents($imagesRoot . 'download.png' , '');

        $downloader->download($url, $imagesRoot, 'download.png');
    }

    public static function downloadFile( $url , $type , $mideaType) 
    {
        switch ($type) {
            case 'post':
                if($mideaType == 'video') {
                    $filePath = HelpUs::getConfigs('Url.basePath') . 'Download/Posts/' . uniqid() . '.mp4';
                }else {
                    $filePath = HelpUs::getConfigs('Url.basePath') . 'Download/Posts/' . uniqid() . '.png';
                }
                break;
            case 'story':
                if($mideaType == 'video') {
                    $filePath = HelpUs::getConfigs('Url.basePath') . 'Download/Stories/' . uniqid() . '.mp4';
                }else {
                    $filePath = HelpUs::getConfigs('Url.basePath') . 'Download/Stories/' . uniqid() . '.png';
                }
                break;
        }

        // Initialize the cURL session
        $ch = curl_init($url);


        // Open file
        $fp = fopen($filePath, 'wb');

        // It set an option for a cURL transfer
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // Perform a cURL session
        curl_exec($ch);

        // Closes a cURL session and frees all resources
        curl_close($ch);

        // Close file
        fclose($fp);

        return basename($filePath);
    }

}