<?php
namespace App\Models;

use PDO;
use App\Core\BaseDatabase;

class Propagend extends BaseDatabase
{
    protected $table = 'propagends';

    public function addMedia($ID , $type , $type_media, $propagend_tag, $owner, $video_view, $like_count, $comment_count ,$date)
    {
        $sql = "INSERT INTO {$this->table} (ID_instagram ,type, type_media, propagend_tag, owner, video_view , like_count, comment_count ,date ) 
        VALUES (:ID_instagram ,:type, :type_media, :propagend_tag, :owner,:video_view , :like_count, :comment_count ,:date)";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':ID_instagram' , $ID);
        $stmt->bindParam(':type' , $type);
        $stmt->bindParam(':type_media' , $type_media);
        $stmt->bindParam(':propagend_tag' , $propagend_tag);
        $stmt->bindParam(':owner' , $owner);
        $stmt->bindParam(':video_view' , $video_view);
        $stmt->bindParam(':like_count' , $like_count);
        $stmt->bindParam(':comment_count' , $comment_count);
        $stmt->bindParam(':date' , $date);
        $stmt->execute();
    }


    public function getMedias($page = 1 , $page_size = 20 , $at = null , $to = null)
    {
        $start = ($page - 1) * $page_size;
        $where = " WHERE created_at BETWEEN '{$at} 00:00:00' AND '{$to} 00:00:00'";
        $sql = "SELECT * FROM {$this->table} ORDER BY created_at DESC";

        if(!is_null($at) AND !is_null($to))
            $sql = "SELECT * FROM {$this->table}".$where."ORDER BY created_at DESC";

        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

        return $result;
    }

    public function getOneMeidaByID( $id )
    {
        $sql = "SELECT * FROM {$this->table} WHERE ID_instagram = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([':id' => $id]);
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function toTrash($id)
    {
        $sql = "UPDATE {$this->table} SET deleted = 1 - deleted WHERE ID = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id',$id);
        $stmt->execute();
    }

    public function deleteMedia($id)
    {
        $sql = "DELETE FROM {$this->table} WHERE ID_instagram = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([':id'=>$id]);
    }
} 