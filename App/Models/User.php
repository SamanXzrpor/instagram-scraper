<?php
namespace App\Models;

use PDO;
use App\Core\BaseDatabase;

class User extends BaseDatabase
{
    protected $table = 'users';

    public function addUser( $username , $email , $password , $number , $gender ,$permission = 'user')
    {
        $sql = "INSERT INTO {$this->table} (user_name , email , password , number, gender , permission ) VALUES 
                (:user_name ,:email ,:password ,:number ,:gender ,:permission)";

        $stmt = $this->db->prepare($sql);

        $stmt->execute([':user_name'=> $username,':email'=>$email,':password'=>$password, ':number'=>$number ,':gender'=>$gender,
                        ':permission'=>$permission ]);
    }

    public function getUser ( $email = null )
    {
        $command = 'SELECT * FROM users';
        $where   = ' WHERE email = :email';

        $sql  = !is_null($email) ? $command . $where : $command;
        $stmt = $this->db->prepare($sql);
        !is_null($email) ? $stmt->execute([':email' => $email]) : $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }
}