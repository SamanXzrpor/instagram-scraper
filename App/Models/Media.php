<?php
namespace App\Models;

use PDO;
use App\Core\BaseDatabase;

class Media extends BaseDatabase
{
    protected $table = 'medias';

    public function addMedia($ID ,$path, $cover_media , $type , $type_media, $propagend_tag, $owner, $video_view, $like_count, $comment_count ,$date)
    {
        $sql = "INSERT INTO {$this->table} (ID ,path, cover_media ,type, type_media, propagend_tag, owner, video_view , like_count, comment_count ,date ,deleted) 
        VALUES (:ID ,:path , :cover_media ,:type, :type_media, :propagend_tag, :owner,:video_view , :like_count, :comment_count ,:date , '0')";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':ID' , $ID);
        $stmt->bindParam(':path' , $path);
        $stmt->bindParam(':cover_media' , $cover_media);
        $stmt->bindParam(':type' , $type);
        $stmt->bindParam(':type_media' , $type_media);
        $stmt->bindParam(':propagend_tag' , $propagend_tag);
        $stmt->bindParam(':owner' , $owner);
        $stmt->bindParam(':video_view' , $video_view);
        $stmt->bindParam(':like_count' , $like_count);
        $stmt->bindParam(':comment_count' , $comment_count);
        $stmt->bindParam(':date' , $date);
        $stmt->execute();
    }

    public function validateMedia( $status, $description , $id) 
    {
        $sql = "UPDATE {$this->table} SET status = :status , description = :description WHERE ID = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':status',$status);
        $stmt->bindParam(':description',$description);
        $stmt->bindParam(':id',$id);
        $stmt->execute();
    }

    public function getMedias($type ,$type_media = null ,$page = 1 , $page_size = 20 , $order_by = 'ASC')
    {
        $start = ($page - 1) * $page_size;

        $sql = "SELECT * FROM {$this->table} WHERE type IN (:type) ORDER BY created_at DESC ";

        $stmt = $this->db->prepare($sql);
        $stmt->execute([':type'=>$type]);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

        return $result;
    }

    public function getOneMeidaByID( $id )
    {
        $sql = "SELECT * FROM {$this->table} WHERE ID = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([':id' => $id]);
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function toTrash($id)
    {
        $sql = "UPDATE {$this->table} SET deleted = 1 - deleted WHERE ID = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id',$id);
        $stmt->execute();
    }

    public function deleteMedia($id)
    {
        $sql = "DELETE FROM {$this->table} WHERE ID = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([':id'=>$id]);
    }
} 