<?php
namespace App\Models;

use PDO;
use App\Core\BaseDatabase;

class RobotData extends BaseDatabase
{
    protected $table = 'robot_data';

    public function addRobot($username , $password , $proxy, $robot_number)
    {
        $sql = "INSERT INTO {$this->table} (user_name, password, proxy, robot_number) 
        VALUES (:user_name, :password, :proxy, :robot_number)";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':user_name' , $username);
        $stmt->bindParam(':password' , $password);
        $stmt->bindParam(':proxy' , $proxy);
        $stmt->bindParam(':robot_number' , $robot_number);
        $stmt->execute();
    }

    public function updateRobot( $user_name , $password , $proxy , $id) 
    {
        $sql = "UPDATE {$this->table} SET user_name = :user_name , password = :password , proxy = :proxy
         WHERE ID = :ID";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':user_name',$user_name);
        $stmt->bindParam(':password',$password);
        $stmt->bindParam(':proxy',$proxy);
        $stmt->bindParam(':ID',$id);
        $stmt->execute();
    }

    public function getRobot($robot_number = null)
    {
        $sql = "SELECT * FROM {$this->table}";
        if(!is_null($robot_number))
            $sql .= " WHERE robot_number = :robot_number";
        

        $stmt = $this->db->prepare($sql);
        
        if(!is_null($robot_number)){
            $stmt->execute([':robot_number' => $robot_number]);
        }else{
            $stmt->execute();
        }
        
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
} 