<div class="pd-ltr-20 xs-pd-20-10">
	<div class="min-height-200px">
        <div class="pd-20 card-box mb-30">
            <form action="<?= $data['url'] . 'ChangeInfo/changeData' ?>" method="POST">
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <div class="form-group">
                            <label>Instagram Username</label>
                            <input type="text" class="form-control" name="username">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="form-group">
                            <label>Instagram Password</label>
                            <input type="text" class="form-control" name="password">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="form-group">
                            <label>Proxy</label>
                            <input type="text" class="form-control" name="proxy">
                        </div>
                    </div>
                </div>
                <span>اگر میخواهید روبات جدیدی بسازید اسم جدیدی با فرمت (robot1,robot2) بنویسید</span>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <input type="text" class="form-control" name="robot_number">
                        </div>
                    </div>
                </div>
                <span>اگر میخواهید فیلدی از روبات های خود را اپدین کنید از اینجا انتخاب کنید</span>
                <div class="form-group row">
                    <label class="col-sm-12 col-md-12 col-form-label"></label>
                    <div class="col-sm-12 col-md-10">
                        <select class="custom-select col-12" name="select-robot">
                            <option selected="">Choose...</option>
                            <?php foreach ($data['robotData'] as $robot):?>
                                <option value="<?= $robot['robot_number'] ?>"><?= $robot['robot_number'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-2">
                    <input type="submit" name="sub-change-info" class="btn btn-success"value="ذخیره">
                </div>
            </form>
        </div>
    
    <br>
    <br>
    <div class="pd-20 card-box mb-30">
            <div class="table-responsive">
                <table class="table table-striped">
                      <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">نام کاربری</th>
                            <th scope="col">رمز عبور</th>
                            <th scope="col">پروکسی</th>
                            <th scope="col">ربات</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($data['robotData'] as $robot ) :?>
                            <tr>
                                <th scope="row"><?= $robot['ID'] ?></th>
                                <td><?= $robot['user_name'] ?></td>
                                <td><?= $robot['password'] ?></td>
                                <td><?= $robot['proxy'] ?></td>
                                <td><?= $robot['robot_number'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="collapse collapse-box" id="responsive-table">
                <div class="code-box">
                    <div class="clearfix">
                        <a href="javascript:;" class="btn btn-primary btn-sm code-copy pull-left"  data-clipboard-target="#responsive-table-code"><i class="fa fa-clipboard"></i> Copy Code</a>
                        <a href="#responsive-table" class="btn btn-primary btn-sm pull-right" rel="content-y"  data-toggle="collapse" role="button"><i class="fa fa-eye-slash"></i> Hide Code</a>
                    </div>
                    <pre><code class="xml copy-pre" id="responsive-table-code">
                        <div class="table-responsive">
                            <table class="table table-striped">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <th scope="row">1</th>
                                </tr>
                            </tbody>
                            </table>
                        </div>
                    </code></pre>
                </div>
            </div>
        </div>
    </div>
</div>