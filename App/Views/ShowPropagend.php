<div class="pd-20 card-box mb-30">
    <form action="<?= $data['url'] . 'PropagendTag/extractPropagendTags' ?>" method="POST">
        <div class="form-group">
            <label>از تاریخ</label>
            <input class="form-control date-picker" placeholder="Select Date" name="at_date" type="text">
        </div>
        <div class="form-group">
            <label>تا</label>
            <input class="form-control date-picker" placeholder="Select Date" name="to_date" type="text">
        </div>
        <button type="submit" name="extractData" class="btn btn-success btn-lg btn-block">Extract</button>
    </form>
</div>
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="pd-20 card-box mb-30">
        <div class="table-responsive">
            <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">پیج تگ شده</th>
                        <th scope="col">پیج تبلیغ کننده</th>
                        <th scope="col">تاریخ انتشار</th>
                        <th scope="col">نوع</th>
                        <th scope="col">لایک/ویو</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data['tags'] as $tag ) :?>
                        <tr>
                            <th scope="row"><?=  $tag->propagend_tag ?></th>
                            <td><?=  $tag->owner ?></td>
                            <td><?= $tag->date ?></td>
                            <td><?= $tag->type ?></td>
                            <td><?= $tag->like_count .' / '. $tag->video_view ?></td>
                            <td><a href="<?= $data['url'] . 'PropagendTag/dltPropagend' ?>?id=<?= $tag->ID_instagram ?>"><button class="btn btn-outline-danger">Remove</button></a></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody> 
            </table>
        </div>
        <div class="collapse collapse-box" id="responsive-table">
            <div class="code-box">
                <div class="clearfix">
                    <a href="javascript:;" class="btn btn-primary btn-sm code-copy pull-left"  data-clipboard-target="#responsive-table-code"><i class="fa fa-clipboard"></i> Copy Code</a>
                    <a href="#responsive-table" class="btn btn-primary btn-sm pull-right" rel="content-y"  data-toggle="collapse" role="button"><i class="fa fa-eye-slash"></i> Hide Code</a>
                </div>
                <pre><code class="xml copy-pre" id="responsive-table-code">
                    <div class="table-responsive">
                        <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                            </tr>
                        </tbody>
                        </table>
                    </div>
                </code></pre>
            </div>
        </div>
    </div>
</div>