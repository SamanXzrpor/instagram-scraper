<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="container pd-0">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="title">
                            <h4>Posts Directory</h4>
                        </div>
                        <nav aria-label="breadcrumb" role="navigation">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= $data['url'] . 'ChangeInfo/index' ?>">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Contact Directory</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="contact-directory-list">
                <ul class="row">
                    <?php foreach ($data['story'] as $story) : ?>
                    <li class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                        <div class="contact-directory-box">
                            <div class="contact-dire-info text-center">
                                <div class="contact-avatar">
                                    <span>
                                        <img src="<?= $data['dlurl'] . $story->cover_media ?>" alt="">
                                    </span>
                                </div>
                                <div class="contact-name">
                                    <h4><?= $story->owner ?></h4>
                                    <p><?= $story->propagend_tag ?></p>
                                    <div class="work text-success"><i class="ion-android-time"></i> <?= date('Y/m/d H:i' ,$story->date) ?> </div>
                                </div>
                                <div class="contact-skill">
                                    <span class="badge badge-pill"><?= $story->like_count ?></span>
                                    <span class="badge badge-pill"><?= $story->video_view ?></span>
                                    <span class="badge badge-pill"><?= $story->comment_count ?></span>
                                </div>
                            </div>
                            <div class="contact-skill text-center">
                                <a href="<?= $data['url'] . 'ShowData/dltMedia' ?>?id=<?= $story->ID ?>"><button type="button" class="btn btn-outline-danger btn-sm">Remove</button></a>
                            </div>
                            <div class="view-contact">
                            <a href="<?= $data['url'] . 'Single/index?ID=' . $story->ID ?>">View More</a>
                            </div>
                        </div>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>