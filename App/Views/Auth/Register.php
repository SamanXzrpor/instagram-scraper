
<?php use App\Helpers\HelpUs; ?>
<!DOCTYPE html>
<html>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>ثبت نام</title>

	<!-- Site favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/images/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/images/favicon-16x16.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/src/plugins/jquery-steps/jquery.steps.css">
	<link rel="stylesheet" type="text/css" href="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/styles/style.css">
	<link rel="stylesheet" type="text/css" href="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/styles/custom.css">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-119386393-1');
	</script>
</head>

<body class="login-page">
	<div class="login-header box-shadow">
		<div class="container-fluid d-flex justify-content-between align-items-center">
			<div class="brand-logo">
				<a href="login.html">
					<img src="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/images/beh.png" alt="">
				</a>
			</div>
			<div class="login-menu">
				<ul>
					<li><a href="<?= $data['url'] . 'Login/index' ?>">Login</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="register-page-wrap d-flex align-items-center flex-wrap justify-content-center">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6 col-lg-7">
					<img src="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/images/register-page-img.png" alt="">
				</div>
				<div class="col-md-6 col-lg-5">
					<div class="register-box bg-white box-shadow border-radius-10">
						<div class="wizard-content">
							<form class="tab-wizard2 wizard-circle wizard" action="<?= $data['url'] . 'Register/registerUser' ?>" method="POST">
								<h5>Basic Account Credentials</h5>
								<section>
									<div class="form-wrap max-width-600 mx-auto">
										<div class="form-group row">
											<label class="col-sm-4 col-form-label">Email Address*</label>
											<div class="col-sm-8">
												<input type="email" name="email" class="form-control" required>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-4 col-form-label">Username*</label>
											<div class="col-sm-8">
												<input type="text" name="username" class="form-control" required>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-4 col-form-label">Password*</label>
											<div class="col-sm-8">
												<input type="password" name="pass" class="form-control" required>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-4 col-form-label">Confirm Password*</label>
											<div class="col-sm-8">
												<input type="password" name="conf-pass" class="form-control" required>
											</div>
										</div>
									</div>
								</section>
								<!-- Step 2 -->
								<h5>Personal Information</h5>
								<section>
									<div class="form-wrap max-width-600 mx-auto">
										<div class="form-group row">
											<label class="col-sm-4 col-form-label">Full Name</label>
											<div class="col-sm-8">
												<input type="text" class="form-control">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-4 col-form-label">Number*</label>
											<div class="col-sm-8">
												<input type="text" name="number" class="form-control" required>
											</div>
										</div>
										<div class="form-group row align-items-center">
											<label class="col-sm-4 col-form-label">Gender*</label>
											<div class="col-sm-8">
												<div class="custom-control custom-radio custom-control-inline pb-0">
													<input type="radio" id="male" name="male" class="custom-control-input">
													<label class="custom-control-label" for="male">Male</label>
												</div>
												<div class="custom-control custom-radio custom-control-inline pb-0">
													<input type="radio" id="female" name="female" class="custom-control-input">
													<label class="custom-control-label" for="female">Female</label>
												</div>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-4 col-form-label">City</label>
											<div class="col-sm-8">
												<input type="text" class="form-control">
											</div>
										</div>
										
									</div>
								</section>
								<button type="submit" id="success-modal-btn" hidden data-toggle="modal" data-target="#success-modal" data-backdrop="static">Launch modal</button>

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- success Popup html Start -->

	<!-- js -->
	<script src="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/scripts/core.js"></script>
	<script src="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/scripts/script.min.js"></script>
	<script src="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/scripts/process.js"></script>
	<script src="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/scripts/layout-settings.js"></script>
	<script src="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/src/plugins/jquery-steps/jquery.steps.js"></script>
	<script src="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/scripts/steps-setting.js"></script>
</body>

</html>