<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="pd-20 card-box mb-30">
            <form method="post" id="uploadPropagend" enctype="multipart/form-data">
                <div class="form-group">
                    <label>فایل مورد نظر را انتخاب کنید</label>
                    <div class="custom-file">
                        <input type="file" name="exceldata" class="custom-file-input">
                        <label class="custom-file-label">Choose file</label>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>ربات مورد نظر را انتخاب کنید</label>
                        <select class="custom-select2 form-control" name="select-robot[]" multiple style="width: 100%;">
                        <?php foreach($data['robots'] as $robot): ?>
                        <option value="<?= $robot['robot_number'] ?>"><?= $robot['robot_number'] ?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <label class="weight-600">Scrap Type</label>
                    <div class="custom-control custom-radio mb-5">
                        <input type="radio" id="customRadio1" name="scrap-post" class="custom-control-input">
                        <label class="custom-control-label" for="customRadio1">Scrap Post</label>
                    </div>
                    <div class="custom-control custom-radio mb-5">
                        <input type="radio" id="customRadio2" name="scrap-story" class="custom-control-input">
                        <label class="custom-control-label" for="customRadio2">Scrap Story</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-success btn-lg btn-block">Start</button>
            </form>
            <br>

            <div class="progress" style="height: 25px;">
                <div class="progress-bar bg-info" id="progress" role="progressbar" style="width: 0%" aria-valuemin="0" aria-valuemax="100">0%</div>
            </div>

            <br>
            <div class="resultScrap" ></div>
        </div>
    </div>
</div>