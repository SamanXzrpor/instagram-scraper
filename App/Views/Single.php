<?php use App\Helpers\HelpUs; ?>
<!DOCTYPE html>
<html>
<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">

	<!-- Site favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/images/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/images/favicon-16x16.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/styles/icon-font.min.css">
	<!-- Slick Slider css -->
	<link rel="stylesheet" type="text/css" href="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/src/plugins/slick/slick.css">
	<!-- bootstrap-touchspin css -->
	<link rel="stylesheet" type="text/css" href="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/src/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.css">
	<link rel="stylesheet" type="text/css" href="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/styles/style.css">
	<link rel="stylesheet" type="text/css" href="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/styles/custom.css">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-119386393-1');
	</script>
</head>
<body>

	<div class="header">
		<div class="header-left">
			<div class="menu-icon dw dw-menu"></div>
			<div class="search-toggle-icon dw dw-search2" data-toggle="header_search"></div>
			<div class="header-search">
				<form>
					<div class="form-group mb-0">
						<i class="dw dw-search2 search-icon"></i>
						<input type="text" class="form-control search-input" placeholder="Search Here">
						<div class="dropdown">
							<a class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
								<i class="ion-arrow-down-c"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right">
								<div class="form-group row">
									<label class="col-sm-12 col-md-2 col-form-label">From</label>
									<div class="col-sm-12 col-md-10">
										<input class="form-control form-control-sm form-control-line" type="text">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-12 col-md-2 col-form-label">To</label>
									<div class="col-sm-12 col-md-10">
										<input class="form-control form-control-sm form-control-line" type="text">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-12 col-md-2 col-form-label">Subject</label>
									<div class="col-sm-12 col-md-10">
										<input class="form-control form-control-sm form-control-line" type="text">
									</div>
								</div>
								<div class="text-right">
									<button class="btn btn-primary">Search</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			<div class="dashboard-setting user-notification">
				<div class="dropdown">
					<a class="dropdown-toggle no-arrow" href="javascript:;" data-toggle="right-sidebar">
						<i class="dw dw-settings2"></i>
					</a>
				</div>
			</div>
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
						<span class="user-icon">
							<img src="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/src/images/photo1.jpg" alt="">
						</span>
						<span class="user-name"><?= $_SESSION['currentUser']->user_name ?></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<a class="dropdown-item" href="<?= $data['url'] . 'Login/logout' ?>"><i class="dw dw-logout"></i> Log Out</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="right-sidebar">
		<div class="sidebar-title">
			<h3 class="weight-600 font-16 text-blue">
				Layout Settings
				<span class="btn-block font-weight-400 font-12">User Interface Settings</span>
			</h3>
			<div class="close-sidebar" data-toggle="right-sidebar-close">
				<i class="icon-copy ion-close-round"></i>
			</div>
		</div>
		<div class="right-sidebar-body customscroll">
			<div class="right-sidebar-body-content">
				<h4 class="weight-600 font-18 pb-10">Header Background</h4>
				<div class="sidebar-btn-group pb-30 mb-10">
					<a href="javascript:void(0);" class="btn btn-outline-primary header-white active">White</a>
					<a href="javascript:void(0);" class="btn btn-outline-primary header-dark">Dark</a>
				</div>

				<h4 class="weight-600 font-18 pb-10">Sidebar Background</h4>
				<div class="sidebar-btn-group pb-30 mb-10">
					<a href="javascript:void(0);" class="btn btn-outline-primary sidebar-light ">White</a>
					<a href="javascript:void(0);" class="btn btn-outline-primary sidebar-dark active">Dark</a>
				</div>

				<h4 class="weight-600 font-18 pb-10">Menu Dropdown Icon</h4>
				<div class="sidebar-radio-group pb-10 mb-10">
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebaricon-1" name="menu-dropdown-icon" class="custom-control-input" value="icon-style-1" checked="">
						<label class="custom-control-label" for="sidebaricon-1"><i class="fa fa-angle-down"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebaricon-2" name="menu-dropdown-icon" class="custom-control-input" value="icon-style-2">
						<label class="custom-control-label" for="sidebaricon-2"><i class="ion-plus-round"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebaricon-3" name="menu-dropdown-icon" class="custom-control-input" value="icon-style-3">
						<label class="custom-control-label" for="sidebaricon-3"><i class="fa fa-angle-double-right"></i></label>
					</div>
				</div>

				<h4 class="weight-600 font-18 pb-10">Menu List Icon</h4>
				<div class="sidebar-radio-group pb-30 mb-10">
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebariconlist-1" name="menu-list-icon" class="custom-control-input" value="icon-list-style-1" checked="">
						<label class="custom-control-label" for="sidebariconlist-1"><i class="ion-minus-round"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebariconlist-2" name="menu-list-icon" class="custom-control-input" value="icon-list-style-2">
						<label class="custom-control-label" for="sidebariconlist-2"><i class="fa fa-circle-o" aria-hidden="true"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebariconlist-3" name="menu-list-icon" class="custom-control-input" value="icon-list-style-3">
						<label class="custom-control-label" for="sidebariconlist-3"><i class="dw dw-check"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebariconlist-4" name="menu-list-icon" class="custom-control-input" value="icon-list-style-4" checked="">
						<label class="custom-control-label" for="sidebariconlist-4"><i class="icon-copy dw dw-next-2"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebariconlist-5" name="menu-list-icon" class="custom-control-input" value="icon-list-style-5">
						<label class="custom-control-label" for="sidebariconlist-5"><i class="dw dw-fast-forward-1"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebariconlist-6" name="menu-list-icon" class="custom-control-input" value="icon-list-style-6">
						<label class="custom-control-label" for="sidebariconlist-6"><i class="dw dw-next"></i></label>
					</div>
				</div>

				<div class="reset-options pt-30 text-center">
					<button class="btn btn-danger" id="reset-settings">Reset Settings</button>
				</div>
			</div>
		</div>
	</div>

	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="index.html">
				<img src="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/images/beh.png" alt="" class="dark-logo">
				<img src="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/images/beh-white.png" alt="" class="light-logo">
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon dw dw-house-1"></span><span class="mtext">خانه</span>
						</a>
						<ul class="submenu">
							<li><a href="<?= $data['url'] . 'ChangeInfo/index' ?>">ایجاد و تغییر ربات</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span  class="micon dw dw-library"></span><span class="mtext">پیج های عمومی</span>
						</a>
						<ul class="submenu">
							<li><a href="<?= $data['url'] . 'ShowData/getScrap' ?>">شروع اسکرپ</a></li>
						</ul>
						<ul class="submenu">
							<li><a href="<?= $data['url'] . 'ShowData/showPosts' ?>">پست ها</a></li>
						</ul>
						<ul class="submenu">
							<li><a href="<?= $data['url'] . 'ShowData/showStories' ?>">استوری ها</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon dw dw-analytics-21"></span><span class="mtext">تعداد تبلیغات</span>
						</a>
						<ul class="submenu">
							<li><a href="<?= $data['url'] . 'PropagendTag/index' ?>">شروع اسکرپ</a></li>
						</ul>
						<ul class="submenu">
							<li><a href="<?= $data['url'] . 'PropagendTag/showPropagend' ?>">نمایش</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>
	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="title">
								<h4>Product Detail</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="index.html">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Product Detail</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
				<div class="product-wrap">
					<div class="product-detail-wrap mb-30"> 
						<div class="row">
							<div class="col-lg-6 col-md-12 col-sm-12">
							<?php if ($data['post'][0]->type_media != 'sidecar'): ?>
									<div class="product-slider slider-arrow">
										<div class="product-slide">
										<?php if (is_int(strpos(pathinfo($data['post'][0]->path, PATHINFO_EXTENSION) , 'png'))): ?>
											<img src="<?= HelpUs::getConfigs('Url.dlUrl') . $data['post'][0]->path ?>" alt="">	
										<?php endif; ?>
										<?php if (is_int(strpos(pathinfo($data['post'][0]->path, PATHINFO_EXTENSION) , 'mp4'))): ?>
											<video controls>
  												<source src="<?= HelpUs::getConfigs('Url.dlUrl') . $data['post'][0]->path ?>" type="video/mp4">
											</video>
										<?php endif; ?>
										</div>
									</div>
									<div class="product-slider-nav">
										<div class="product-slide">
										<?php if (is_int(strpos(pathinfo($data['post'][0]->path, PATHINFO_EXTENSION) , 'png'))): ?>
											<img src="<?= HelpUs::getConfigs('Url.dlUrl') . $data['post'][0]->path ?>" alt="">	
										<?php endif; ?>
										<?php if (is_int(strpos(pathinfo($data['post'][0]->path, PATHINFO_EXTENSION) , 'mp4'))): ?>
											<img src="<?= HelpUs::getConfigs('Url.dlUrl') . $data['post'][0]->cover_media ?>">
										<?php endif; ?>
										</div>
									</div>
								<?php endif; ?>
								<?php if ($data['post'][0]->type_media == 'sidecar'): ?>
									<div class="product-slider slider-arrow">
									<?php foreach (unserialize($data['post'][0]->path) as $path): ?>
										<div class="product-slide">
										<?php if (is_int(strpos(pathinfo($path, PATHINFO_EXTENSION) , 'png'))): ?>
											<img src="<?= HelpUs::getConfigs('Url.dlUrl') . $path ?>" alt="">	
										<?php endif; ?>
										<?php if (is_int(strpos(pathinfo($path, PATHINFO_EXTENSION) , 'mp4'))): ?>
											<video controls>
  												<source src="<?= HelpUs::getConfigs('Url.dlUrl') . $path ?>" type="video/mp4">
											</video>										
										<?php endif; ?>
										</div>
									<?php endforeach; ?>
									</div>
									<div class="product-slider-nav">
									<?php foreach (unserialize($data['post'][0]->path) as $path): ?>
										<div class="product-slide">
										<?php if (is_int(strpos(pathinfo($path, PATHINFO_EXTENSION) , 'png'))): ?>
											<img src="<?= HelpUs::getConfigs('Url.dlUrl') . $path ?>" alt="">	
										<?php endif; ?>
										<?php if (is_int(strpos(pathinfo($path, PATHINFO_EXTENSION) , 'mp4'))): ?>
											<img src="<?= HelpUs::getConfigs('Url.dlUrl') . $data['post'][0]->cover_media ?>">
										<?php endif; ?>
										</div>
									<?php endforeach;?>
									</div>
								<?php endif; ?>
							</div>
							<div class="col-lg-6 col-md-12 col-sm-12">
								<div class="contact-directory-box">
                            		<div class="contact-dire-info text-center">
										<div class="contact-name">
												<h4><?= $data['post'][0]->owner ?></h4>
												<br>
												<p><?= $data['post'][0]->propagend_tag ?></p>
												<br>												
												<div class="work text-success"><i class="ion-android-time"></i> <?= date('m/d H:i' ,$data['post'][0]->date) ?> </div>
											<div class="contact-skill">
												<span class="badge badge-pill">like :<?= $data['post'][0]->like_count ?></span>
												<span class="badge badge-pill">view :<?= $data['post'][0]->video_view ?></span>
												<span class="badge badge-pill">comment :<?= $data['post'][0]->comment_count ?></span>
											</div>
											
										</div>
										<div class="contact-skill text-center">
											<a href="<?= $data['url'] . 'ShowData/dltMedia' ?>?id=<?= $data['post'][0]->ID ?>"><button type="button" class="btn btn-outline-danger btn-sm">Remove</button></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- js -->
	<script src="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/scripts/core.js"></script>
	<script src="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/scripts/script.min.js"></script>
	<script src="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/scripts/process.js"></script>
	<script src="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/vendors/scripts/layout-settings.js"></script>
	<script src="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/src/plugins/slick/slick.min.js"></script>
	<!-- bootstrap-touchspin js -->
	<script src="<?= HelpUs::getConfigs('Url.baseUrl') ?>Public/src/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
	<script>
		jQuery(document).ready(function() {
			jQuery('.product-slider').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: true,
				infinite: true,
				speed: 1000,
				fade: true,
				asNavFor: '.product-slider-nav'
			});
			jQuery('.product-slider-nav').slick({
				slidesToShow: 3,
				slidesToScroll: 1,
				asNavFor: '.product-slider',
				dots: false,
				infinite: true,
				arrows: false,
				speed: 1000,
				centerMode: true,
				focusOnSelect: true
			});
			$("input[name='demo3_22']").TouchSpin({
				initval: 1
			});
		});
	</script>
</body>
</html>