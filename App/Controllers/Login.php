<?php
namespace App\Controllers;

use \App\Helpers\HelpUs;
use \App\Core\Controller;
use \App\Models\User;

class Login extends Controller
{

    public function index ()
    {
        $data = ['url' => HelpUs::getConfigs('Url.baseUrl') ];
        $this->getView('Auth.Login' , 'auth' , $data);
    }

    public function loginUser ()
    {
        $userMode = new User();

        if($_SERVER['REQUEST_METHOD'] == 'POST') {

            if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {

                $userByEmail = $userMode->getUser($_POST['email'])[0];
                if(!empty($userByEmail) AND $userByEmail->email == $_POST['email']) {
    
                    if(password_verify($_POST['pass'], $userByEmail->password)) {
    
                        $_SESSION['currentUser'] = $userByEmail;
    
                        if($userByEmail->permission == 'admin') {
                            header("Location: ". HelpUs::getConfigs('Url.baseUrl'). 'ChangeInfo/index');
                        }else{
                            header("Location: ". HelpUs::getConfigs('Url.baseUrl'). 'Home/index');
                        }
                    }else{
                        die('Password dont Correct' );
                    }
                }else{
                    die('This Email Not Registerd' );
                }
            }else{
                die('Your Email not Valid');
            }
        }
    }

    public static function logined()
    {
        return isset($_SESSION['currentUser']) ? true : false;
    }

    public static function logout()
    {
        session_destroy();
        header("Location: ". HelpUs::getConfigs('Url.baseUrl'). 'Login/index');

    }

}