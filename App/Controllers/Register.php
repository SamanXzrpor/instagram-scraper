<?php
namespace App\Controllers;

use \App\Helpers\HelpUs;
use \App\Core\Controller;
use \App\Models\User;

class Register extends Controller
{

    public function index ()
    {
        $data = ['url' => HelpUs::getConfigs('Url.baseUrl') ];
        $this->getView('Auth.Register' , 'auth' , $data);
    }

    public function registerUser ()
    {
        $userModel = new User();

        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $userName = $_POST['username'];
            $email    = $_POST['email'];
            $pass     = $_POST['pass'];
            $confPass = $_POST['conf-pass'];
            $number   = $_POST['number'];

            if (isset($_POST['male']) && $_POST['male'] == 'on'){
                $gender = 'male';
            }elseif (isset($_POST['female']) && $_POST['female'] == 'on') {
                $gender = 'female';
            }

            if(!empty($userName) AND !empty($email)){
                if(filter_var($email , FILTER_VALIDATE_EMAIL)){
                    if($pass == $confPass){

                        $userModel->addUser($userName , $email , password_hash($pass , PASSWORD_BCRYPT) , $number , $gender);
                        HelpUs::redirect('Login/index');
                    }else{
                        echo 'رمز خود را به درستی وارد کنید';
                    }
                }else{
                    echo 'ایمیل شما درست نیست';
                }
            }else{
                echo 'فیلد های لازم را پر کنید';
            }
        }
    }

}