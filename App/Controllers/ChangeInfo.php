<?php
namespace App\Controllers;

use \App\Core\Controller;
use \App\Helpers\HelpUs;
use \App\Models\RobotData;


if(!Login::logined()) 
    header('Location: Login/index');

    
class ChangeInfo extends Controller
{

    public function index ()
    {
        $robotModel = new RobotData();
        $robots = $robotModel->getRobot();
        $data = ['url'=>HelpUs::getConfigs('Url.baseUrl') , 'robotData' => $robots];
        $this->getView('ChangeInfo' , 'default' , $data);
    }

    public function changeData()
    {
        if(isset($_POST['sub-change-info']))
        {
            $robot    = $_POST['select-robot'];
            # check This Robot Exsit Or Not
            $robotModel = new RobotData();
            $exitRobot  = $robotModel->getRobot($robot);

            $robotNum = !empty($_POST['robot_number']) ? $_POST['robot_number'] : $exitRobot[0]['robot_number'];
            $userName = !empty($_POST['username']) ? $_POST['username'] : $exitRobot[0]['user_name'];
            $password = !empty($_POST['password']) ? $_POST['password'] : $exitRobot[0]['password'];
            $proxy    = !empty($_POST['proxy'])    ? $_POST['proxy']    : $exitRobot[0]['proxy'];
  

            if(empty($exitRobot)){
                $robotModel->addRobot($userName , $password , $proxy , $robotNum);
            }else{

                $robotModel->updateRobot($userName , $password , $proxy , $exitRobot[0]['ID']);
            }
            header('Location: ' . HelpUs::getConfigs('Url.baseUrl') . 'ChangeInfo/index');
        }
    }

}