<?php
namespace App\Controllers;

use \App\Core\Controller;
use \App\Models\Media;
use \App\Helpers\HelpUs;
use App\Services\ExcelHandler;
use App\Services\InstagramScraper;
use App\Models\RobotData;
use \App\Utilities\Pagination;

if(!Login::logined()) 
    header('Location: Login/index');

class ShowData extends Controller
{

    public function getScrap ()
    {
        $robotModel = new RobotData();
        $robots = $robotModel->getRobot();

        $data = ['url'=>HelpUs::getConfigs('Url.baseUrl') , 'robots'=>$robots];
        $this->getView('GetScrap' , 'default' , $data);
    }

    public function showPosts ()
    {
        if (isset($_GET['page'])) {
            $process = $_GET['page'];

            $page = Pagination::paginate($process);

            var_dump($page);
            // exit;
        }

        $mediaHandler = new Media();
        $posts = $mediaHandler->getMedias('post');
        $data = ['url'=>HelpUs::getConfigs('Url.baseUrl'),'dlurl'=>HelpUs::getConfigs('Url.dlUrl') , 'posts'=>$posts];
        $this->getView('ListPosts' , 'default' , $data);
    }

    public function showStories ()
    {
        $mediaHandler = new Media();
        $story = $mediaHandler->getMedias('story');
        $data = ['url'=>HelpUs::getConfigs('Url.baseUrl') ,'dlurl'=>HelpUs::getConfigs('Url.dlUrl') ,'story'=>$story];
        $this->getView('ListStories' , 'default' , $data);
    }

    public function startScrap()
    {
        if(isset($_POST)){
          
            # Get Excel Data
            if (!isset($_FILES['exceldata'])) {
                return 'File Not Choosed';
            }
            
            $file = isset($_FILES['exceldata']) ? $_FILES['exceldata']['tmp_name'] : null;
            $type = explode('.' , $_FILES['exceldata']['name'])[1];

            # Get Robots That Selected
            $robots = isset($_POST['select-robot']) ? $_POST['select-robot'] : null;
            # Ditermine Type of scrap for Post Or story
            $scrapPost  = isset($_POST['scrap-post'])  ? $_POST['scrap-post'] : null;
            $scrapStory = isset($_POST['scrap-story']) ? $_POST['scrap-story'] : null;

            # Get Data of Acc that extract at Excel 
            $accountData = ExcelHandler::getDataOfExcel(ucfirst($type) , $file);

            # Get All Robot Data
            $robotModel = new RobotData();
            
            if(is_float(count($accountData) / count($robots))) {

                $afterDat = explode('.' , strval(count($accountData) / count($robots)))[1];

                if(intval(substr($afterDat, 0 ,2)) > 50) {
                    $accountData [] = ['saman_xp' , '500'];
                }
                if(intval(substr($afterDat, 0 ,2)) == 50) {
                    $accountData [] = ['saman_xp' , '500'];
                    $accountData [] = ['saman_xp' , '500'];
                }
                if(intval(substr($afterDat, 0 ,2)) < 50) {
                    array_pop($accountData);
                }
            }
            
            # Divide List of accounts
            $allLists = array_chunk($accountData , count($accountData)/count($robots));

            $data = array_combine($robots , $allLists);
           
            foreach ($data as $key => $list) {
                $info = $robotModel->getRobot($key)[0];
                $username = $info['user_name'];
                $pass = $info['password'];
                if(!is_null($scrapPost) AND $scrapPost == 'on') {
                    $scrap = new InstagramScraper($info['proxy'] ,[$username , $pass] ,$list);
                    $scrap->posts();
                }
                if(!is_null($scrapStory) AND $scrapStory == 'on') {
                    $scrap = new InstagramScraper($info['proxy'] ,[$username , $pass] ,$list);
                    $scrap->stories();
                }
            }

            header('Location: getScrap');
        }
    }

    public function dltMedia ()
    {
        $mediaModel = new Media();

        if (isset($_GET['id']) AND !empty($_GET['id'])) {

            $ID   = $_GET['id'];
            $type = $mediaModel->getOneMeidaByID($ID)[0]->type;
            $mediaModel->deleteMedia($ID);

            if($type == 'post') {
                header('Location: showPosts');
            }else{
                header('Location: showStories');
            }
        }
    }

}