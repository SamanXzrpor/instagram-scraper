<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Helpers\HelpUs;
use App\Models\Media;

if(!Login::logined()) 
    header('Location: Login/index');
    
class Single extends Controller
{

    public function index ()
    {
        $mediaModel = new Media();
        if(isset($_GET['ID']))
            $ID = $_GET['ID'];


        $mediaData = $mediaModel->getOneMeidaByID($ID);
        $data = ['url'=>HelpUs::getConfigs('Url.baseUrl') , 'post' => $mediaData];
        $this->getView('Single' , 'sinfle' , $data);
    }

}