<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Helpers\HelpUs;
use App\Models\Propagend;
use App\Services\ExcelHandler;
use App\Services\GetPropagendTag;
use App\Models\RobotData;

if(!Login::logined()) 
    header('Location: Login/index');

    
class PropagendTag extends Controller 
{    
    public $data = [];

    public function index()
    {
        $robotModel = new RobotData();
        $robots = $robotModel->getRobot();

        $this->data = ['url'=>HelpUs::getConfigs('Url.baseUrl') , 'robots' => $robots ];
        $this->getView('PropagendTag' , 'default' ,$this->data);
    }
    
    public function scrapPropagends ()
    {
        if(isset($_POST)){

            # Get Robots That Selected
            $robots = $_POST['select-robot'];
            # Get Excel Data
            $file = $_FILES['exceldata']['tmp_name'];
            $type = explode('.' , $_FILES['exceldata']['name'])[1];

            # Ditermine Type of scrap for Post Or story
            $scrapPost  = isset($_POST['scrap-post'])  ? $_POST['scrap-post'] : null;
            $scrapStory = isset($_POST['scrap-story']) ? $_POST['scrap-story'] : null;

            # Get Data of Acc that extract at Excel 
            $accountData = ExcelHandler::getDataOfExcel(ucfirst($type) , $file);

            # Divide List of accounts
            $allLists = array_chunk($accountData , count($accountData)/count($robots));

            # Get All Robot Data
            $robotModel = new RobotData();
            if(is_float(count($accountData) / count($robots))) {

                $afterDat = explode('.' , strval(count($accountData) / count($robots)))[1];

                if(intval(substr($afterDat, 0 ,2)) > 50) {
                    $accountData [] = ['saman_xp' , '500'];
                }
                if(intval(substr($afterDat, 0 ,2)) == 50) {
                    $accountData [] = ['saman_xp' , '500'];
                    $accountData [] = ['saman_xp' , '500'];
                }
                if(intval(substr($afterDat, 0 ,2)) < 50) {
                    array_pop($accountData);
                }
            }
            
            # Divide List of accounts
            $allLists = array_chunk($accountData , count($accountData)/count($robots));

            $data = array_combine($robots , $allLists);
           
            foreach ($data as $key => $list) {
                $info = $robotModel->getRobot($key)[0];
                $username = $info['user_name'];
                $pass = $info['password'];
                if(!is_null($scrapPost) AND $scrapPost == 'on') {
                    $scrap = new GetPropagendTag($info['proxy'] ,[$username , $pass] ,$list);
                    $scrap->posts();
                }
                if(!is_null($scrapStory) AND $scrapStory == 'on') {
                    $scrap = new GetPropagendTag($info['proxy'] ,[$username , $pass] ,$list);
                    $scrap->stories();
                }
            }
        }
    }

    public function showPropagend ()
    {
        $mediaHandler = new Propagend();
        $tags = $mediaHandler->getMedias();

        $this->data = ['url'=>HelpUs::getConfigs('Url.baseUrl') , 'tags'=>$tags ];
        $this->getView('ShowPropagend' , 'default' ,$this->data);
    }

    public function extractPropagendTags ()
    {
        $prpagendModel = new Propagend();
        $filename = HelpUs::getConfigs('Url.basePath') .'Download/excel.xlsx';

        if(isset($_POST['extractData'])) {
            $at = date('Y-m-d' , strtotime($_POST['at_date']));
            $to = date('Y-m-d' , strtotime($_POST['to_date']));

            $dataToExtarct = $prpagendModel->getMedias( 1 , 20 , $at , $to);
            if(empty($dataToExtarct))
               echo 'داده ای یافت نشد';
               
            # Save Data in Excel File
            $excelHandler = new ExcelHandler();
            $excelHandler->createExcel($dataToExtarct);
            
            # Download Excel File
            header('Content-Description: File Transfer');
            header('Content-Type: application/xlsx');
            header("Content-Disposition: attachment; filename=\"" . basename($filename) . "\";");
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filename));
            ob_clean();
            flush();
            readfile($filename); 
        }
    }

    public function dltPropagend ()
    {
        $propagendModel = new Propagend();

        if (isset($_GET['id']) AND !empty($_GET['id'])) {

            $ID = $_GET['id'];
            $propagendModel->deleteMedia($ID);

            header('Location: showPropagend');
        }
    }
  
}
 
