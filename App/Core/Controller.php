<?php
namespace App\Core;


class Controller  
{

    public function getModel(string $model)
    {
        $modelsDir = __DIR__ . '/../Models/' . $model . '.php';
        if(class_exists($modelsDir))
            include $modelDir;

        return null;
    }

    public function getView(string $path,string $leyout = 'default' ,array $data = [])
    {
        $sections = explode('.',$path);

        $basePath = __DIR__ . '/../Views/';

        $fileKey = array_key_last($sections);

        foreach($sections as $section){

            if($section   == $sections[$fileKey]){

                $basePath .= $sections[$fileKey] . '.php';
            }else{

                $basePath .= $section . '/';
            }
        }

        if($leyout !== 'default'){
            include $basePath;
            exit;
        }


        $leyout = __DIR__ . '/../Views/Leyouts/'.$leyout.'.php';

        include $leyout;
    }
}