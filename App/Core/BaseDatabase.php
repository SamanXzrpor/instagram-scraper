<?php
namespace App\Core;

use PDO;
use PDOException;
use App\Helpers\HelpUs;
class BaseDatabase 
{
    protected $db ;
    protected $table;

    public function __construct ()
    {
        $host = HelpUs::getConfigs('Database.host');
        $user = HelpUs::getConfigs('Database.user');
        $pass = HelpUs::getConfigs('Database.password');
        $dbname = HelpUs::getConfigs('Database.dbname');
        try {
            $this->db = new PDO("mysql:host={$host};dbname={$dbname}" , $user , $pass);
            $this->db->setAttribute(PDO::ATTR_ERRMODE , PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            die('Database ERROR : '.$e->getMessage());
        }
    }
}