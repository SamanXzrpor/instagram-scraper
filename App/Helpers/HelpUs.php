<?php
namespace App\Helpers;


class HelpUs
{

    public static function getConfigs(string $config)
    {
        $parts = explode('.' , $config);
        $configPath = __DIR__ . '/../Configs/';
        $file = $configPath . $parts[0] .'.php';

        $data = include($file);
        
        if(array_key_exists(1 , $parts))
            return $data[$parts[1]];

        return $data;
    }
    

    public static function Changer($arr, &$proxyCounter)
    { 
        $proxyCounter++;

        return $arr[$proxyCounter] ?? '195.137.203.242:80';
    }

    public static function redirect( $url )
    {
        header("Location : $url");
    }

    public static function timeHelper(string $date)
    {
        if($date >= '31404000') {
            return date('Y سال , m ماه و H ساعت قبل' , $date);
        }else if($date >= '2592000') {
            return date('m ماه و H ساعت و i دقیقه قبل' , $date);
        } else if($date >= '86400') {
            return date('d روز و H ساعت  قبل' , $date);
        } else {
            return date('H ساعت و i دقیقه قبل' , $date);
        }
    }
} 
